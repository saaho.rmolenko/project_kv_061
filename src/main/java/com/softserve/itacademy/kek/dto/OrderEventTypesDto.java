package com.softserve.itacademy.kek.dto;

public enum OrderEventTypesDto {
    CREATED,
    ASSIGNED,
    STARTED,
    DELIVERED
}
