package com.softserve.itacademy.kek.models;

public interface IOrderEventType {

    String getName();
}
